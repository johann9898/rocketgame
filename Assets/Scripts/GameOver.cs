using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject gameOverUi;
    private float tempFuelRate;
    void Start()
    {
        gameOverUi.SetActive(false);
        tempFuelRate = GameManager.instance.FuelRate;
    }

    private void Update()
    {
        if (GameManager.instance.gameOver)
        {
            GameManager.instance.FuelRate = 0f;
            gameOverUi.gameObject.SetActive(true);
            Time.timeScale = 0f;
        }
    }
    public void Menu()
    {
        Time.timeScale = 1f;
        GameManager.instance.gameOver = false;
        GameManager.instance.alive = true;
        GameManager.instance.FuelRate = tempFuelRate;
        SceneManager.LoadScene("LevelSelector");
    }
}
