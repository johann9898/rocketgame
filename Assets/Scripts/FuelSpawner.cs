using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelSpawner : MonoBehaviour
{   
    public GameObject Fuel;
    void Start()
    {
        StartCoroutine(SpawnFuel());
    }


    IEnumerator SpawnFuel()
    {
        yield return new WaitForSeconds(GameManager.instance.fuelSpawnRate);
        Collider2D[] collider = Physics2D.OverlapCircleAll(this.gameObject.transform.parent.localPosition, 10f);
        int itemCounter = 0;
        if (collider.Length > 0)
        {
            foreach (var item in collider)
            {
                if (item.tag == "Fuel")
                {
                    itemCounter++;
                }
            }
            if (itemCounter == 0)
            {
                if (GameManager.instance.fuelSpawnTotal > 0)
                {
                    GameManager.instance.fuelSpawnTotal -= 1;
                    Instantiate(Fuel);
                }
            }
        }
        else
        {
            if (GameManager.instance.fuelSpawnTotal > 0)
            {
                GameManager.instance.fuelSpawnTotal -= 1;
                Instantiate(Fuel);
            }
        }
        StartCoroutine(SpawnFuel());
       

    }
}
