using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MeterBar : MonoBehaviour
{
    public Slider slider;
    // Start is called before the first frame update
    public Gradient gradient;
    public Image fill;
    public void SetMaxFuel(float fuel)
    {
        slider.maxValue = fuel;
        slider.value = fuel;
        fill.color = gradient.Evaluate(1f);
    }
    public void SetFuel(float fuel)
    {
        slider.value = fuel;
        fill.color = gradient.Evaluate(slider.normalizedValue);
    }
}
