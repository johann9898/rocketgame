using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    public GameObject Coin;
    void Start()
    {
        StartCoroutine(SpawnCoin());
    }

    
    IEnumerator SpawnCoin()
    {
        yield return new WaitForSeconds(GameManager.instance.coinSpawnRate);
        Collider2D[] collider = Physics2D.OverlapCircleAll(this.gameObject.transform.parent.localPosition, 10f);
        int itemCounter = 0;

        if (collider.Length > 0)
        {
            foreach (var item in collider)
            {
                if (item.tag == "Coin")
                {
                    itemCounter++;
                }
            }
            if (itemCounter == 0)
            {
                if (GameManager.instance.coinSpawnTotal > 0)
                {
                    GameManager.instance.coinSpawnTotal -= 1;
                    Instantiate(Coin);
                }
            }
        }
        else
        {
            if (GameManager.instance.coinSpawnTotal > 0)
            {
                GameManager.instance.coinSpawnTotal -= 1;
                Instantiate(Coin);
            }
        }
        StartCoroutine(SpawnCoin());

    }
}
