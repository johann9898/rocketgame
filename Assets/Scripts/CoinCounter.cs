using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCounter : MonoBehaviour
{
    public TMPro.TextMeshProUGUI coinsLabel;

    // Update is called once per frame
    void Update()
    {
        coinsLabel.text = "Coins: " + GameManager.instance.coinCollected.ToString();
    }
}
