using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelTest : MonoBehaviour
{
    private int maxFuel = 10;
    private int currentFuel;
    public MeterBar fuelBar;
    // Start is called before the first frame update
    void Start()
    {
        currentFuel = maxFuel;
        fuelBar.SetMaxFuel(maxFuel);
        StartCoroutine(Health());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator Health()
    {
        
        yield return new WaitForSeconds(5f);
        currentFuel -= 1;
        fuelBar.SetFuel(currentFuel);
        StartCoroutine(Health());
    }
}
