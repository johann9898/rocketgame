using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    private bool explode = false;
    public Animator animator;
    public GameObject fuelBarObj;
    public GameObject fade;
    public GameObject healthGameobj;
    public GameObject nitroObj;
    public GameObject nitro;
    public Sprite[] healthSprites;
    private int health = 4;
    private float speed;
    private float baseSpeed;
    private Animator fadeAnim;
    private MeterBar fuelBar;
    private MeterBar nitroBar;
    private float fuelRateMovement;
    public GameObject rocketFire;
    public bool nitroChecker;
    private Rigidbody2D rb;
    public bool hit;
    private bool invincible;
    public AudioClip[] CometSounds;
    public AudioClip[] AsteroidsSounds;
    public AudioClip[] PlanetSounds;
    public AudioSource Hit;

    // Update is called once per frame
    private void Start()
    {
        hit = false;
        invincible = false;
        speed = 0f;
        baseSpeed = 7.0f;
        fuelRateMovement = GameManager.instance.FuelRate / 7f;
        rb = GetComponent<Rigidbody2D>();

        if (PlayerPrefs.GetInt("nitroUpgrade") > 0)
        {
            nitroObj.gameObject.SetActive(true);
            nitro.gameObject.SetActive(true);
            nitroChecker = true;
            nitroBar = nitro.GetComponent<MeterBar>();
            GameManager.instance.nitroCapacity = 500f * PlayerPrefs.GetInt("nitroUpgrade");
            nitroBar.SetMaxFuel(GameManager.instance.nitroCapacity);

        }
        else
        {
            nitroObj.gameObject.SetActive(false);
            nitro.gameObject.SetActive(false);
            nitroChecker = false;
        }
        fadeAnim = fade.GetComponent<Animator>();
        fuelBar = fuelBarObj.GetComponent<MeterBar>();
        GameManager.instance.currentFuel = PlayerPrefs.GetFloat("maxFuel") * (PlayerPrefs.GetFloat("fuelUpgrade") * 0.2f + 1f);
        fuelBar.SetMaxFuel(PlayerPrefs.GetFloat("maxFuel"));
    }
    void FixedUpdate()
    {
        if (hit == true)
        {
            hit = false;
            StartCoroutine(hitBlinker());
        }

        if (!explode && GameManager.instance.canMove && GameManager.instance.distance > 0)
        {
            transform.Rotate(0, 0, (-180 * Time.fixedDeltaTime) * Input.GetAxis("Horizontal"));
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                speed += 0.1f;
            }
            else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                speed -= 0.5f;
            }
            if (Input.GetKey(KeyCode.Space) && nitroChecker)
            {

                speed += 0.1f;
                rocketFire.gameObject.GetComponent<Animator>().SetBool("Nitro", true);
                rocketFire.GetComponent<TrailRenderer>().startColor = new Color(0f, 1f, 0.9411765f);
                rocketFire.GetComponent<TrailRenderer>().endColor = new Color(0f, 0.5215687f, 1f);
                float nitroSpeed = (baseSpeed + (baseSpeed * 0.3f)) + ((PlayerPrefs.GetInt("nitroUpgrade")-1) * (baseSpeed * 0.2f));
                if (speed > nitroSpeed)
                {
                    speed = nitroSpeed;
                }
                GameManager.instance.nitroCapacity -= 1f * speed;
                nitroBar.SetFuel(GameManager.instance.nitroCapacity);
            }
            else
            {
                rocketFire.gameObject.GetComponent<Animator>().SetBool("Nitro", false);
                rocketFire.GetComponent<TrailRenderer>().startColor = new Color(1f, 0.9268422f, 0f);
                rocketFire.GetComponent<TrailRenderer>().endColor = new Color(1f, 0.2529368f, 0f);
                if (speed > baseSpeed)
                {
                    speed = baseSpeed;
                }
            }

            if (GameManager.instance.nitroCapacity <= 0)
            {
                nitroChecker = false;
            }
            if (speed < 0.0f)

            {
                speed = 0f;
            }

            rb.velocity = transform.up * speed;
            //transform.position += transform.up * Time.deltaTime * speed;

        }
        Exhaust();
        if (GameManager.instance.currentFuel > 0)
        {
            GameManager.instance.currentFuel -= fuelRateMovement * speed;
            fuelBar.SetFuel(GameManager.instance.currentFuel);
        }
        else
        {
            GameManager.instance.gameOverText = "Your ran out of fuel!";
            Explode();
        }

    }
    void Update()
    {

        rb.angularVelocity = 0f;
        if (GameManager.instance.distance <= 0)
        {
            fadeAnim.SetTrigger("Fade");      
        }
        else
        {
            if (health <= 1)
            {
                healthGameobj.GetComponent<Image>().sprite = healthSprites[0];
                GameManager.instance.gameOverText = "Your ran out of health!";
                Explode();
            }
            else
            {
                healthGameobj.GetComponent<Image>().sprite = healthSprites[healthSprites.Length - health];
            }

        }


    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.CompareTag("BigAsteroid"))
        {
            if (!invincible)
            {
                if(AsteroidsSounds.Length > 0)
                {
                    Hit.PlayOneShot(AsteroidsSounds[0]);
                }
                hit = true;
                invincible = true;
                GameManager.instance.damaged = true;
                transform.rotation = Quaternion.LookRotation(Vector3.forward,(transform.position - other.transform.position).normalized);
                rb.velocity = transform.up;
                health -= 2;

            }
        }
        if (other.gameObject.tag == "Asteroid")
        {
            if (!invincible)
            {
                if (AsteroidsSounds.Length > 0)
                {
                    Hit.PlayOneShot(AsteroidsSounds[1]);
                }
                hit = true;
                invincible = true;
                GameManager.instance.damaged = true;
                other.transform.GetComponent<Rigidbody2D>().isKinematic = true;
                other.transform.GetComponent<Collider2D>().enabled = false;
                other.transform.GetComponent<SpriteRenderer>().enabled = false;
                other.transform.Find("AsteroidExplosionParticle").gameObject.SetActive(true);
                Destroy(other.gameObject, 1f);
                health -= 1;

            }
 
        }

        if (other.transform.CompareTag("MediumAsteroid"))
        {
            if (!invincible)
            {
                if (AsteroidsSounds.Length > 0)
                {   
                        Hit.PlayOneShot(AsteroidsSounds[2]);   
                }

                hit = true;
                invincible = true;
                GameManager.instance.damaged = true;
                other.transform.GetComponent<Rigidbody2D>().isKinematic = true;
                other.transform.GetComponent<Collider2D>().enabled = false;
                other.transform.GetComponent<SpriteRenderer>().enabled = false;
                other.transform.Find("AsteroidExplosionParticle").gameObject.SetActive(true);
                health -= 1;
                other.gameObject.GetComponent<MediumAsteroid>().splitAsteroid();
            }
        }
        if (other.transform.CompareTag("Health"))

        {
            health += 1;
            if (health > 4)
            {
                health = 4;
            }
        }
        if (other.transform.CompareTag("Planet"))
        {
            if (PlanetSounds.Length > 0)
            {
                Hit.PlayOneShot(PlanetSounds[0]);
            }

            fadeAnim.SetTrigger("Fade");
            if (PlayerPrefs.GetInt("planetUnlocked") <= GameManager.instance.planetLevel)
            {
                PlayerPrefs.SetInt("planetUnlocked", PlayerPrefs.GetInt("planetUnlocked") + 1);
            }
        }

    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("BlueComet")){
      
            if (!invincible)
            {
                if (CometSounds.Length > 0)
                {
                    Hit.PlayOneShot(CometSounds[0]);
                }
                hit = true;
                invincible = true;
                GameManager.instance.damaged = true;
                health -= 1;
            }
        }


        if (collision.CompareTag("YellowComet"))
        {
            if (!invincible)
            {
                if (CometSounds.Length > 0)
                {
                    Hit.PlayOneShot(CometSounds[0]);
                }
                hit = true;
                invincible = true;
                GameManager.instance.damaged = true;
                health -= 1;
            }
        }
    }
    

    void Explode()
    {
        explode = true;
        gameObject.transform.localScale = new Vector2(1, 1);
        GameManager.instance.alive = false;
        GameManager.instance.gameOver = true;
        transform.Find("NormalFlame").gameObject.SetActive(false);
        animator.SetTrigger("explosion");
        transform.Find("ExplosionParticle").gameObject.SetActive(true);
    }

    void Exhaust()
    {
        float scale = 0f;
        float yPosition = 0f;
        if (speed > 5f)
        {
            scale = 0.5f;
            yPosition = -2.6f;
        }
        else if (speed > 4f)
        {
            scale = 0.4f;
            yPosition = -2.57f;
        }
        else if (speed > 3f)
        {
            scale = 0.3f;
            yPosition = -2.36f;
        }
        else if (speed > 2f)
        {
            scale = 0.2f;
            yPosition = -2.27f;
        }
        else if (speed > 1f)
        {
            scale = 0.1f;
            yPosition = -2.13f;
        }
        else
        {
            scale = 0.0f;
            yPosition = -2f;
        }
        
        rocketFire.transform.localScale = new Vector3(scale, scale, scale);
        rocketFire.transform.localPosition = new Vector3(0.032f, yPosition, 1);
    }

    IEnumerator hitBlinker()
    {
        for (int i = 0; i < 5; i++)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
            yield return new WaitForSeconds(0.2f);
            gameObject.GetComponent<Renderer>().material.color = Color.white;
            yield return new WaitForSeconds(0.2f);
        }
        invincible = false;
    }

}
