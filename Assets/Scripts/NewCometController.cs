using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewCometController : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject rocket;
    public bool isDestroyed;
    public Vector2 direction;
    public bool isSplit;
    public bool isVarient;
    public GameObject Comet_split;
    public GameObject particle;
    public TrailRenderer trail;
    public bool isfollow;
    Rigidbody2D body;
    public float time = 2;


    void Start()
    {
        rocket = GameManager.instance.player;
        float impulse = Random.Range(5f, 10f);
        Vector2 direction = -transform.right + -transform.up;
        body.velocity = direction.normalized * impulse;
        transform.rotation.SetLookRotation(body.velocity);
        particle.transform.localScale = gameObject.transform.localScale * 0.14f;
    }

    void Awake()
    {
        body = gameObject.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        trail.startWidth = gameObject.transform.localScale.x * 0.1f;
        float distance = Vector2.Distance(rocket.transform.position, transform.position);
        if (distance > 30f)
        {
            isSplit = false;
            Destroy(this.gameObject);
        }

        if (distance < 10f)
        {
            SplitComet();
        }
    }

    private void FixedUpdate()
    {
        if (isfollow)
        {
            if (time > 0)
            {
                direction = (rocket.transform.position - transform.position).normalized;
                moveComet(direction);
                time -= Time.fixedDeltaTime;
            }
            else
            {
                isfollow = false;
            }
        }
    }

    public void moveComet(Vector2 direction)
    {
        if (isDestroyed)
        {
            body.velocity = direction * 0f * Time.fixedDeltaTime;
        }
        else
        {
            body.velocity = direction * 200f * Time.fixedDeltaTime;
            transform.up = direction;
        }
    }

    public void SpawnCluster(Vector2 clusterPos,int rotate, int id)
    {
        GameObject Cometsplit = Instantiate(Comet_split, clusterPos, transform.rotation);
        Cometsplit.gameObject.transform.rotation = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y,
            transform.eulerAngles.z+ rotate);

        Cometsplit.gameObject.transform.localScale += new Vector3(gameObject.transform.localScale.x / 5.0f,
            gameObject.transform.localScale.y / 5.0f, 1.0f);
        var comitsplit = Cometsplit.gameObject.GetComponent<NewCometController>();
        comitsplit.body.velocity = body.velocity;

    }

    public void SplitComet()
    {
        if (isSplit && !isVarient)
        {
            isSplit = false;
            SpawnCluster(new Vector3(transform.position.x-1f, transform.position.y, 1.0f), + 30, 1);
            SpawnCluster(new Vector3(transform.position.x+1f, transform.position.y, 1.0f), - 30, 2);
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (isSplit)
        {
            if (collision.tag == "YellowComet" && !isVarient)
            {
                transform.localScale += collision.gameObject.transform.localScale;
                if (!collision.gameObject.GetComponent<NewCometController>().isDestroyed)
                {
                    isDestroyed = true;
                }
            }
        }
        else
        {
            if (collision.tag == "MediumAsteroid" && gameObject.tag == "BlueComet")
            {
                collision.gameObject.GetComponent<MediumAsteroid>().splitAsteroid();
                GetDestroyed();
            }
        }

        if (collision.tag == "Rocket")
        {

            GetDestroyed();
        }
    }
    void GetDestroyed()
    {
        isDestroyed = true;
        body.velocity = direction * 0 * Time.fixedDeltaTime;
        gameObject.transform.GetComponent<Rigidbody2D>().isKinematic = true;
        if (gameObject.tag == "BlueComet")
        {
            gameObject.transform.Find("GameObject").GetComponent<Collider2D>().enabled = false;
            gameObject.transform.Find("GameObject").GetComponent<SpriteRenderer>().enabled = false;
            gameObject.transform.GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            gameObject.transform.GetComponent<Collider2D>().enabled = false;
            gameObject.transform.GetComponent<SpriteRenderer>().enabled = false;
        }
        gameObject.transform.Find("MeteorExplosionParticle").gameObject.SetActive(true);
        Destroy(this.gameObject, 1f);
    }
}
