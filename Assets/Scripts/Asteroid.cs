using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    private Rigidbody2D body;
    private Vector2 direction;
    private GameObject rocket;
    public GameObject particle;
    private void Start()
    {
        particle.transform.localScale = gameObject.transform.localScale;
        float impulse;

        rocket = GameManager.instance.player;
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, gameObject.transform.localScale.x * 2);
        foreach (var item in collider)
        {
            if (item.tag != "Rocket" && item.gameObject != gameObject)
            {
                Destroy(this.gameObject);
            }
        }
        body = transform.GetComponent<Rigidbody2D>();
        direction = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f,1f));
        if (gameObject.transform.localScale.x > 2.5f)
        {
            impulse = Random.Range(0f, 1f);
        }
        else
        {
            impulse = Random.Range(0f, 2f);
        }
        body.velocity = direction * impulse;


    }
    private void Update()
    {
        transform.rotation *= Quaternion.Euler(Vector3.forward * 5f * Time.deltaTime);
        float distance = Vector2.Distance(rocket.transform.position, transform.position);
        if (distance > 30f)
        {
            Destroy(this.gameObject);
        }
    }

    private void onCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Rocket")
        {
            //transform.GetComponent<SpriteRenderer>().enabled = false;
            //transform.Find("AsteroidExplosionParticle").gameObject.SetActive(true);
            //transform.Find("AsteroidExplosionParticle").gameObject.transform.localScale = gameObject.transform.localScale * 1.5f;
            //Destroy(this.gameObject, 0.15f);
        }
    }
}
