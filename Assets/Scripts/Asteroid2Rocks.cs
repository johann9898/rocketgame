using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid2Rocks : MonoBehaviour
{
    private Rigidbody2D body;
    private Vector2 direction;
    private GameObject rocket;
    private Vector3 targetAngles;

    private void Start()
    {
        float impulse;
        rocket = GameManager.instance.player;
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, transform.localScale.x / 5f);
        foreach (var item in collider)
        {
            if (item.tag != "Rocket" && item.gameObject != gameObject)
            {
                Destroy(this.gameObject);
            }
        }
        body = transform.GetComponent<Rigidbody2D>();
        direction = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        if (gameObject.transform.localScale.x > 25f)
        {
            impulse = Random.Range(0f, 1f);
        }
        else
        {
            impulse = Random.Range(0f, 2f);
        }
        body.velocity = direction * impulse;


    }
    private void Update()
    {
        transform.rotation *= Quaternion.Euler(Vector3.forward * 5f * Time.deltaTime);
        float distance = Vector2.Distance(rocket.transform.position, transform.position);
        if (distance > 30f)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Rocket")
        {
            //rocket.gameObject.transform.rotation.(rocket.gameObject.transform.position - transform.position);
            //Debug.Log(rocket.gameObject.transform.rotation.z);
            //var tempVect = rocket.gameObject.transform.position;

            //rocket.gameObject.transform.rotation = new Vector3(0, 0, rocket.gameObject.transform.eulerAngles.z + 180f);
            //targetAngles = rocket.gameObject.transform.eulerAngles + 180f * Vector3.up;

            //rocket.gameObject.transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, targetAngles, 1f * Time.deltaTime);

            //transform.GetComponent<SpriteRenderer>().enabled = false;
            //transform.Find("AsteroidExplosionParticle").gameObject.SetActive(true);
            //transform.Find("AsteroidExplosionParticle").gameObject.transform.localScale = gameObject.transform.localScale * 0.2f;
            //Destroy(this.gameObject, 0.15f);
        }
    }
}
