using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceCounter : MonoBehaviour
{
    public TMPro.TextMeshProUGUI coinsLabel;

    // Update is called once per frame
    void Update()
    {
        Vector2 rocketPos = GameManager.instance.player.transform.position;
        GameObject planetPos = GameManager.instance.planet;
        float planetRadius = planetPos.GetComponent<CircleCollider2D>().radius;
        int distance = Mathf.RoundToInt((Vector2.Distance(rocketPos, planetPos.transform.position) - (13f)) * 1000f);
        if (distance < 0)
        {
            distance = 0;
        }
        GameManager.instance.distance = distance;
        coinsLabel.text = "Destination: \n" + distance.ToString() + "KM";
    }
}
