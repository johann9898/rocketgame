using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenuUI;
    private float tempFuelRate;
    // Update is called once per frame
    void Start()
    {
        pauseMenuUI.SetActive(false);
        tempFuelRate = GameManager.instance.FuelRate;


    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !GameManager.instance.gameOver)
        {
            if (pauseMenuUI.gameObject.activeSelf)
            {
                Continue();
            }
            else
            {
                Pause();
            }
        }
    }
    public void Continue()
    {
        Time.timeScale = 1f;
        GameManager.instance.FuelRate = tempFuelRate;
        pauseMenuUI.SetActive(false);
    }
    public void Pause()
    {
        pauseMenuUI.SetActive(true);
        GameManager.instance.FuelRate = 0f;
        Time.timeScale = 0f;
    }
    public void MainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }
}
