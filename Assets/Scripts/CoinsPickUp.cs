using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsPickUp : MonoBehaviour
{
    private GameObject rocket;
    private Rigidbody2D body;
    private Vector2 direction;
    public AudioSource coinSound;

    private void Start()
    {
        Collider2D[] collider;
        rocket = GameManager.instance.player;
        Vector3 min;
        Vector3 max;
        min = (rocket.transform.up * (8f + 10f)) + rocket.transform.position;
        max = (rocket.transform.up * (9f + 10f)) + rocket.transform.position;
        float x = Random.Range(min.x, max.x);
        float y = Random.Range(min.y, max.y);
        Vector3 randomPos = new Vector3(x, y, 1);

        transform.position = new Vector3(randomPos.x, randomPos.y, 1);
        collider = Physics2D.OverlapCircleAll(transform.position, 0.3f);
        foreach (var item in collider)
        {
            if (item.tag != "Rocket")
            {
                GameManager.instance.coinSpawnTotal += 1;
                Destroy(this.gameObject);
            }
        }
        body = transform.GetComponent<Rigidbody2D>();
        direction = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        float impulse = Random.Range(0f, 1f);
        body.velocity = direction * impulse;
    }
    private void Update()
    {
        //transform.rotation *= Quaternion.Euler(Vector3.forward * 3f * Time.deltaTime);
        float distance = Vector2.Distance(rocket.transform.position, transform.position);
        if (distance > 30f)
        {
            GameManager.instance.coinSpawnTotal += 1;
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Rocket")
        {
            if (!coinSound.isPlaying)
            {
                coinSound.Play();
            }
            GameManager.instance.currentFuel += PlayerPrefs.GetFloat("maxFuel") * 0.02f;
            GameManager.instance.coinCollected += 10;
            PlayerPrefs.SetInt("balance", PlayerPrefs.GetInt("balance") + 10);
            transform.gameObject.GetComponent<Collider2D>().enabled = false;
            transform.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            Destroy(this.gameObject, 2f);
        }
    }
}
