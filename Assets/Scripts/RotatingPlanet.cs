using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingPlanet : MonoBehaviour
{
    public GameObject outliner;
    private SpriteRenderer renderer;

    private void Start()
    {
        renderer = outliner.GetComponent<SpriteRenderer>();
        StartCoroutine(BlinkingOutline());
    }
    void Update()
    {
        transform.rotation *= Quaternion.Euler(Vector3.forward * 2f * Time.deltaTime);
    }
    IEnumerator BlinkingOutline()
    {
        renderer.enabled = false;
        yield return new WaitForSeconds(0.3f);
        renderer.enabled = true;
        yield return new WaitForSeconds(0.3f);
        StartCoroutine(BlinkingOutline());
    }
}
