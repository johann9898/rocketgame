using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMenu : MonoBehaviour
{
    public GameObject control;

    public void Control()
    {
        control.SetActive(true);
    }

    public void ExitControl()
    {
        control.SetActive(false);
    }
}
