using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelSelectorManager : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] planets;
    void Start()
    {
        int planetLevel = PlayerPrefs.GetInt("planetUnlocked");

        for (int i = 0; i <= planetLevel; i++)
        {
            planets[i].GetComponent<Button>().interactable = true;
        }
    }

}
