using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UpgradeBalance : MonoBehaviour
{
    public TextMeshProUGUI balanceText;
    // Start is called before the first frame update
    void Start()
    {
        balanceText.text = "X" + PlayerPrefs.GetInt("balance").ToString();
    }

    // Update is called once per frame
    void Update()
    {
        balanceText.text = "X" + PlayerPrefs.GetInt("balance").ToString();
    }
}
