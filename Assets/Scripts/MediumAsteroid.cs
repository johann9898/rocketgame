using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MediumAsteroid : MonoBehaviour
{

    private Rigidbody2D body;
    private Vector2 direction;
    private GameObject rocket;
    private Vector3 targetAngles;
    public GameObject clusterPref;
    public GameObject particle;
    public bool shouldSplit;
    private bool isQuitting;
    public bool isVariant;

    // Start is called before the first frame update
    void Start()
    {
        float impulse;
        particle.transform.localScale = gameObject.transform.localScale * 0.075f;
        rocket = GameManager.instance.player;
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, gameObject.transform.localScale.x / 5f);
        foreach (var item in collider)
        {
            if (item.tag != "Rocket" && item.gameObject != gameObject)
            {
                shouldSplit = false;
                Destroy(this.gameObject);
            }
        }
        body = transform.GetComponent<Rigidbody2D>();
        direction = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));

        impulse = Random.Range(0f, 2f);
        body.velocity = direction * impulse;

    }

    // Update is called once per frame
    private void Update()
    {
        if(rocket != null)
        {
            shouldSplit = true;
            transform.rotation *= Quaternion.Euler(Vector3.forward * 5f * Time.deltaTime);
            float distance = getDistance();
            if (distance > 30f)
            {
                shouldSplit = false;
                gameObject.transform.GetComponent<Rigidbody2D>().isKinematic = true;
                gameObject.transform.GetComponent<Collider2D>().enabled = false;
                gameObject.transform.GetComponent<SpriteRenderer>().enabled = false;
                gameObject.transform.Find("AsteroidExplosionParticle").gameObject.SetActive(true);
                Destroy(this.gameObject);
            }
        }


    }

    public float getDistance()
    {
        return Vector2.Distance(rocket.transform.position, transform.position);
    }


    public void spawnCluster(Vector2 clusterPos)
    {
        GameObject clusterPrefs = Instantiate(clusterPref, clusterPos, transform.rotation);
        clusterPrefs.gameObject.transform.localScale += new Vector3(gameObject.transform.localScale.x / 3.0f, gameObject.transform.localScale.y / 3.0f, 1.0f);
    }

    public void splitAsteroid()
    {
        // create 3 copies of the cluster prefab
        if (rocket.gameObject != null)
        {
            if (shouldSplit && !isVariant)
            {
                shouldSplit = false;

                spawnCluster(new Vector3(transform.position.x + 1.5f, transform.position.y - 1.5f, 1.0f));
                spawnCluster(new Vector3(transform.position.x - 1.5f, transform.position.y + 1.5f, 1.0f));
                spawnCluster(new Vector3(transform.position.x + 1.5f, transform.position.y + 1.5f, 1.0f));

            }
            gameObject.transform.GetComponent<Rigidbody2D>().isKinematic = true;
            gameObject.transform.GetComponent<Collider2D>().enabled = false;
            gameObject.transform.GetComponent<SpriteRenderer>().enabled = false;
            gameObject.transform.Find("AsteroidExplosionParticle").gameObject.SetActive(true);
            Destroy(gameObject, 1f);
        }

    }


    /*void onCollisionEnter2D(Collider2D other)
    {
        Destroy(this.gameObject, 0.15f);
        GameObject a = Instantiate(this.gameObject) as GameObject;
        a.gameObject.transform.localScale += new Vector3(this.gameObject.transform.localScale.x / 3f, this.gameObject.transform.localScale.x / 3f, this.gameObject.transform.localScale.x / 3f);
        a.transform.position = new Vector2(this.gameObject.transform.position.x - 3f, this.gameObject.transform.position.y + 5f);
    }*/
}
