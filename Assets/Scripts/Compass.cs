using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compass : MonoBehaviour
{
    public Transform endPlanet;
    public Transform Player;
    public float speed = 100000.0f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        Vector3 dir = endPlanet.position - Player.position;

        transform.up = dir;
        
    }
}
