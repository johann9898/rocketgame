using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBackground : MonoBehaviour
{
    public GameObject PlayerCamera;
    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.alive)
        {
            
            if (PlayerCamera.transform.position.y <= this.transform.position.y - (GetComponent<SpriteRenderer>().bounds.size.y))
            {
                transform.position += new Vector3(0, -(GetComponent<SpriteRenderer>().bounds.size.y * 2f) + 0.3f, 0);
            }
            if (PlayerCamera.transform.position.y >= this.transform.position.y + (GetComponent<SpriteRenderer>().bounds.size.y))
            {
                transform.position += new Vector3(0, (GetComponent<SpriteRenderer>().bounds.size.y * 2f) - 0.3f, 0);
            }
            if (PlayerCamera.transform.position.x <= this.transform.position.x - (GetComponent<SpriteRenderer>().bounds.size.x))
            {
                transform.position += new Vector3(-(GetComponent<SpriteRenderer>().bounds.size.x * 2) + 0.3f, 0, 0);
            }
            if (PlayerCamera.transform.position.x >= this.transform.position.x + (GetComponent<SpriteRenderer>().bounds.size.x))
            {
                transform.position += new Vector3((GetComponent<SpriteRenderer>().bounds.size.x * 2) - 0.3f, 0, 0);
            }
        }
        
    }
}
