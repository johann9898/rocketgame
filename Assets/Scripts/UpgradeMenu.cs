using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeMenu : MonoBehaviour
{
    public GameObject UpgradeObject;
    public GameObject UpgradeBar;
    public Button ButtonUpgrade;
    public TextMeshProUGUI textBar;
    public bool Fuel;
    public bool Nitro;
    private int UpgradeCost;
    public Sprite bar0;
    public Sprite bar1;
    public Sprite bar2;
    public Sprite bar3;
    public Sprite bar4;
    // Start is called before the first frame update
    void Start()
    {
        UpgradeCost = 500 + (GetUpgradeType() * 200);
        if (ButtonUpgrade != null)
        {
            ButtonUpgrade.onClick.AddListener(onBuy);
            textBar.text = "X" + UpgradeCost;
            changeBarLevel(GetUpgradeType());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        UpgradeCost = 500 + (GetUpgradeType() * 200);
        changeBarLevel(GetUpgradeType());
        textBar.text = "X" + UpgradeCost;
    }
   
    public void onBuy()
    {
        
        if (PlayerPrefs.GetInt("balance") >= UpgradeCost)
        {
            if (UpgradeObject.gameObject.tag == "Nitro")
            {
                PlayerPrefs.SetInt("nitroUpgrade", PlayerPrefs.GetInt("nitroUpgrade") + 1);
                PlayerPrefs.SetInt("balance", PlayerPrefs.GetInt("balance") - UpgradeCost);
            }
            if (UpgradeObject.gameObject.tag == "FuelUpgrade")
            {
                PlayerPrefs.SetInt("fuelUpgrade", PlayerPrefs.GetInt("fuelUpgrade") + 1);
                Debug.Log(PlayerPrefs.GetInt("fuelUpgrade"));
                PlayerPrefs.SetInt("balance", PlayerPrefs.GetInt("balance") - UpgradeCost);
            }
            if (UpgradeObject.gameObject.tag == "Shield")
            {
                Debug.Log("Shield");
            }
            
        }
        else
        {
            NotEnoughCredit();
        }
    }

    int GetUpgradeType()
    {
        int upgradeType = 0;
        if (Fuel)
        {
            upgradeType = PlayerPrefs.GetInt("fuelUpgrade");
        }
        else if (Nitro)
        {
            upgradeType = PlayerPrefs.GetInt("nitroUpgrade");
        }
        return upgradeType;
    }

    void changeBarLevel(int barLevel)
    {
        if(barLevel == 1)
        {
            UpgradeBar.GetComponent<SpriteRenderer>().sprite = bar1;
        }
        if(barLevel == 2)
        {
            UpgradeBar.GetComponent<SpriteRenderer>().sprite = bar2;
        }
        if (barLevel == 3)
        {
            UpgradeBar.GetComponent<SpriteRenderer>().sprite = bar3;
        }
        if (barLevel == 4)
        {
            UpgradeBar.GetComponent<SpriteRenderer>().sprite = bar4;
        }

    }

    void NotEnoughCredit()
    {
        Debug.Log("Not enough Coins!");
    }
}
