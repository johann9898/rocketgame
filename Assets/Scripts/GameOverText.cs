using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverText : MonoBehaviour
{
    public TMPro.TextMeshProUGUI gameOverText;
    // Start is called before the first frame update
    void Update()
    {
        gameOverText.text = GameManager.instance.gameOverText;
    }
}
