using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameObject player;
    public GameObject planet;
    public int planetLevel;
    public int coinCollected;
    public bool gameOver = false;
    public bool canMove;
    public float FuelRate;
    public string scenePlanetEnd;
    public float currentFuel;
    public float nitroCapacity;
    public bool alive = true;
    public int distance = 1;
    public string gameOverText;
    public bool damaged;
    public float asteroidSpawnRate;
    public float coinSpawnRate;
    public float fuelSpawnRate;
    public float healthSpawnRate;
    public int coinSpawnTotal;
    public int fuelSpawnTotal;
    public int healthSpawnTotal;

    void Awake()
    {
        instance = this;
        damaged = false;
    }
}
