using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStart : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        if (!PlayerPrefs.HasKey("nitroUpgrade"))
        {
            PlayerPrefs.SetInt("nitroUpgrade", 0);
        }
        if (!PlayerPrefs.HasKey("fuelUpgrade"))
        {
            PlayerPrefs.SetInt("fuelUpgrade", 0);
        }

        if (!PlayerPrefs.HasKey("intro"))
        {
            PlayerPrefs.SetInt("intro", 0);
        }
        if (!PlayerPrefs.HasKey("maxFuel"))
        {
            PlayerPrefs.SetFloat("maxFuel", 3500f);
        }
        if (!PlayerPrefs.HasKey("balance"))
        {
            PlayerPrefs.SetInt("balance", 0);
        }
        if (!PlayerPrefs.HasKey("planetUnlocked"))
        {
            PlayerPrefs.SetInt("planetUnlocked", 0);
        }
    }
}
