using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidSpawner : MonoBehaviour
{
    public GameObject[] asteroidList;
    private float randomSize = 0f;
    void Start()
    {
        if (asteroidList.Length > 0)
        {
            StartCoroutine(SpawnAsteroid());

        }
    }

    IEnumerator SpawnAsteroid()
    {
        yield return new WaitForSeconds(GameManager.instance.asteroidSpawnRate);
        Collider2D[] collider = Physics2D.OverlapCircleAll(this.gameObject.transform.parent.localPosition, 10f);

        int randomAsterod = Random.Range(0, asteroidList.Length);
        int itemCounter = 0;

        if (collider.Length > 0)
        {
            foreach (var item in collider)
            {
                if (item.tag == "BigAsteroid" || item.tag == "MediumAsteroid" || item.tag == "Asteroid")
                {
                    itemCounter++;
                }
            }
            if (itemCounter == 0)
            {
                Asteroid(randomAsterod);
            }
        }
        else
        {
            Asteroid(randomAsterod);
        }
        StartCoroutine(SpawnAsteroid());
    }

    void Asteroid(int randomAsterod)
    {
        GameObject rocket = GameManager.instance.player;

        Vector3 min = new Vector3(0f, 0f, 0f);
        Vector3 max = new Vector3(0f, 0f, 0f);
        
        if (asteroidList[randomAsterod].tag == "Asteroid")
        {
            randomSize = Random.Range(0.2f, 0.5f);
            min = (rocket.transform.up * (8f + 10f)) + rocket.transform.position;
            max = (rocket.transform.up * (9f + 10f)) + rocket.transform.position;
        }
        else if (asteroidList[randomAsterod].tag == "BigAsteroid")
        {   

            randomSize = Random.Range(25f, 40f);
            min = (rocket.transform.up * (10f + 10f)) + rocket.transform.position;
            max = (rocket.transform.up * (20f + 10f)) + rocket.transform.position;
        }
        else if (asteroidList[randomAsterod].tag == "MediumAsteroid")
        {
            randomSize = Random.Range(8f, 18f);
            if (randomSize >= 15)
            {
                min = (rocket.transform.up * (10f + 10f)) + rocket.transform.position;
                max = (rocket.transform.up * (15f + 10f)) + rocket.transform.position;
            }
            else
            {
                min = (rocket.transform.up * (8f + 10f)) + rocket.transform.position;
                max = (rocket.transform.up * (10f + 10f)) + rocket.transform.position;
            }
        }     
        float x = Random.Range(min.x, max.x);
        float y = Random.Range(min.y, max.y);
        Vector3 randomPos = new Vector3(x, y, 1);
        Vector3 spawnPos = new Vector3(randomPos.x, randomPos.y, 1);
        GameObject newAsteroid = Instantiate(asteroidList[randomAsterod]);
        newAsteroid.transform.position = spawnPos;
        newAsteroid.transform.localScale += new Vector3(randomSize, randomSize, randomSize);
        newAsteroid.transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
    }
}
