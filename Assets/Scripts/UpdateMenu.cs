using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UpdateMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public Button mainMenu;
    // Update is called once per frame
    private void Start()
    {
        mainMenu.onClick.AddListener(MainMenu);

    }
    void MainMenu()
    {
        SceneManager.LoadScene("MainMenu"); 
    }
}
