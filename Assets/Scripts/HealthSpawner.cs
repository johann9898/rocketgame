using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSpawner : MonoBehaviour
{
    public GameObject Health;
    void Start()
    {
        StartCoroutine(SpawnHealth());
    }

    
    IEnumerator SpawnHealth()
    {
        yield return new WaitForSeconds(GameManager.instance.healthSpawnRate);
        Collider2D[] collider = Physics2D.OverlapCircleAll(this.gameObject.transform.parent.localPosition, 10f);
        int itemCounter = 0;

        if (collider.Length > 0)
        {
            foreach (var item in collider)
            {
                if (item.tag == "Health")
                {
                    itemCounter++;
                }
            }
            if (itemCounter == 0)
            {
                if (GameManager.instance.healthSpawnTotal > 0)
                {
                    GameManager.instance.healthSpawnTotal -= 1;
                    Instantiate(Health);
                }
            }
        }
        else
        {
            if (GameManager.instance.healthSpawnTotal > 0)
            {
                GameManager.instance.healthSpawnTotal -= 1;
                Instantiate(Health);
            }
        }
        StartCoroutine(SpawnHealth());

    }
}
