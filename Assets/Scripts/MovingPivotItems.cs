using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPivotItems : MonoBehaviour
{
        // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.alive == true)
        {
            transform.position += -GameManager.instance.player.transform.up * Time.deltaTime * 5;
        }
    }
}
