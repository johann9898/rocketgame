using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ExplainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject levelEnabler;
    public GameObject continueBtn;
    public GameObject introChecker;
    public void Continue()
    {
        PlayerPrefs.SetInt("intro", PlayerPrefs.GetInt("intro") + 1);
        levelEnabler.SetActive(true);
        continueBtn.GetComponent<Button>().interactable = false;
        introChecker.GetComponent<IntroChecker>().contEnabler = false;
        this.gameObject.SetActive(false);
    }
    
}
