using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraMove : MonoBehaviour
{
    void FixedUpdate()
    {

        Vector3 playerPos = GameManager.instance.player.gameObject.transform.position;
        bool nitroChecker = GameManager.instance.player.gameObject.GetComponent<Player>().nitroChecker;
        if (GameManager.instance.damaged == true)
        {
            GameManager.instance.damaged = false;
            StartCoroutine(ShakeDamage(0.5f));
        }

        if (nitroChecker && Input.GetKey(KeyCode.Space))
        {
            float randomX = Random.Range(-0.1f, 0.1f);
            transform.position = new Vector3(playerPos.x + randomX, playerPos.y + 2, -10f);
        }
        else
        {
            transform.position = new Vector3(playerPos.x, playerPos.y + 2, -10f);
        }

    }
    IEnumerator ShakeDamage(float duration)
    {
        float elapsed = 0.0f;
        while (elapsed < duration)
        {
            float randomX = Random.Range(-0.5f, 0.5f);
            transform.position = new Vector3(GameManager.instance.player.gameObject.transform.position.x + randomX, GameManager.instance.player.gameObject.transform.position.y + 2, -10f);
            elapsed += Time.deltaTime;

            yield return null;
        }
        transform.position = new Vector3(GameManager.instance.player.gameObject.transform.position.x, GameManager.instance.player.gameObject.transform.position.y + 2, -10f);
    }
}
