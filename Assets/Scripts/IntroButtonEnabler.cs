using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroButtonEnabler : MonoBehaviour
{
    // Start is called before the first frame update
    private Button button;
    void Start()
    {
        button = gameObject.GetComponent<Button>();
        StartCoroutine(ButtonEnabler());
    }

    // Update is called once per frame
    IEnumerator ButtonEnabler()
    {
        yield return new WaitForSeconds(4f);
        button.interactable = true;

    }
}
