using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RingEnabler : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        GameObject planetWithRing = gameObject.transform.parent.gameObject;
        Button ring = planetWithRing.GetComponent<Button>();
        if (ring.interactable)
        {
            gameObject.GetComponent<Button>().interactable = true;
        }
        else
        {
            gameObject.GetComponent<Button>().interactable = false;
        }
    }
}
