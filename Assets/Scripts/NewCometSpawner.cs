using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewCometSpawner : MonoBehaviour
{
    public GameObject[] comet;
    public float spawnRadius;
    public float spawnRate = 0.01f;
    void Start()
    {
        if (comet.Length > 0)
        {
            Invoke("SpawnAsteroid", spawnRate);
        }
    }
    void SpawnAsteroid()
    {
        int randomComet = Random.Range(0, comet.Length);
        var spawnPoint = (Vector2)GameManager.instance.player.transform.position + UnityEngine.Random.insideUnitCircle.normalized * spawnRadius;
        var size = Random.Range(1f, 2f);
        var cometInstance = Instantiate(comet[Random.Range(0,2)], spawnPoint, Quaternion.identity);
        cometInstance.transform.up = spawnPoint - (Vector2)GameManager.instance.player.transform.position + UnityEngine.Random.insideUnitCircle.normalized * Random.Range(0f, 3f);
        cometInstance.transform.localScale *= size;
        Invoke("SpawnAsteroid", 2f);
    }
}
