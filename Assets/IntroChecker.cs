using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntroChecker : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject explainMenu;
    public GameObject upgrade;
    public GameObject explainBtn;
    public GameObject contibtn;
    public GameObject mainMenu;
    public GameObject levelSelector;
    public bool contEnabler;
    void Start()
    {
        contEnabler = false;
        if (PlayerPrefs.GetInt("intro") > 0)
        {
            explainMenu.SetActive(false);
            levelSelector.SetActive(true);
        }
        else
        {
            explainMenu.SetActive(true);
            levelSelector.SetActive(false);

        }
    }

    private void Update()
    {
        if (!explainMenu.activeSelf)
        {
            upgrade.GetComponent<Button>().interactable = true;
            mainMenu.GetComponent<Button>().interactable = true;
            explainBtn.GetComponent<Button>().interactable = true;
        }
        else
        {
            upgrade.GetComponent<Button>().interactable = false;
            mainMenu.GetComponent<Button>().interactable = false;
            explainBtn.GetComponent<Button>().interactable = false;
            if (!contEnabler)
            {
                contEnabler = true;
                StartCoroutine(ButtonEnabler());
            }
        }
    }

    public void ExplainMenu()
    {
        explainMenu.SetActive(true);
    }

    IEnumerator ButtonEnabler()
    {
        yield return new WaitForSeconds(4f);
        contibtn.GetComponent<Button>().interactable = true;
    }
}
